@echo off 
mkdir project
cd project
@REM echo "Downloading elastic search"
@REM curl -sS https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.13.2-windows-x86_64.zip > file.zip

echo "Cloning elastic search"
git clone https://terakoya_academia:9EMLx5WFTk4ykAELeBEp@gitlab.com/terakoya_academia/prokan-pf-bat-file.git
echo "Going to prokan folder"
cd prokan-pf-bat-file

echo "Renaming elasticsearch.zip to filename.zip"
ren C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\prokan-pf-bat-file\elasticsearch-7.13.2.zip file.zip

echo "Moving filename.zip to project folder"
move C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\prokan-pf-bat-file\file.zip C:\Users\Administrator\Desktop\prokan-pf-bat-file\project

cd C:\Users\Administrator\Desktop\prokan-pf-bat-file\project
echo "unzipping file..."
"C:/Program Files/7-Zip/7z.exe" x file.zip

@REM Print present working director
cd C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\elasticsearch-7.13.2\config

@REM Removing jvm.options file from config folder
del jvm.options

@REM Move file from Downloads to config folder
move C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\prokan-pf-bat-file\jvm.options C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\elasticsearch-7.13.2\config\

@REM RUN ElasticSearch on background
cd C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\elasticsearch-7.13.2\bin
call elasticsearch-plugin install analysis-kuromoji
timeout /t 10

@REM Start Elastic search on background
start elasticsearch
timeout /t 10

echo "Elastic search Running"

@REM Cloning prokan pf on project repository
cd C:\Users\Administrator\Desktop\prokan-pf-bat-file\project
git clone https://terakoya_academia:9EMLx5WFTk4ykAELeBEp@gitlab.com/terakoya_academia/prokanpf.git

@REM Install npm services
cd prokanpf
call npm install 
call npm run build /N
move C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\prokanpf\dist\prokan-pf.new C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\prokanpf\ServerApplication\
cd C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\prokanpf\ServerApplication\
ren prokan-pf.new prokan-pf

timeout /t 10

echo "Angular program Running"

@REM Creating Virtual Environment for dependencies
echo "Creating virtual environment"
cd C:\Users\Administrator\Desktop\prokan-pf-bat-file\project
python -m venv env
echo "activating virtual environment"
cd env\Scripts
call activate

echo "Python virtual environment Running"

@REM Installing requirements dependencies
echo "Starting requirements"
pip3 install -r ../../prokanpf/requirements.txt
echo "Requirements installation finished"
call pip freeze
echo "Pip list"

echo "Creating log folder"
cd ../../prokanpf/ServerApplication
mkdir log
cd prokanpf_server 
echo "Run server.py file"
start python server.py

@REM echo "Python server Running"
@REM cd C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\prokanpf\ServerApplication\prokanpf_server
cd ../../ManagementTools/create_minimum_data
echo "Initiating deleting, creating and loading of system data"
python 0_delete_all_index.py
echo "0_ completed"
python 1_create_index.py
echo "1_ completed"
python 2_load_system_data.py
echo "2_ completed"
echo "Done"

cd C:\Program Files\nginx-1.20.1
start nginx -s reload