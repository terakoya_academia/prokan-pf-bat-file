@echo off

npx kill-port 9000

cd project

echo "Cloning elastic search"
@REM git clone https://terakoya_academia:9EMLx5WFTk4ykAELeBEp@gitlab.com/terakoya_academia/prokan-pf-bat-file.git
echo "Going to prokan folder"
cd prokan-pf-bat-file

echo "Checking out develop branch"
git checkout develop

@REM RUN ElasticSearch on background
cd C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\elasticsearch-7.13.2\bin

@REM Start Elastic search on background
start elasticsearch
timeout /t 10

echo "Elastic search Running"
@REM Cloning prokan pf on project repository
cd C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\prokanpf

echo "Pulling Master branch of Prokan PF"
git pull origin master

call npm install
call npm run build /N
move C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\prokanpf\dist\prokan-pf.new C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\prokanpf\ServerApplication\
cd C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\prokanpf\ServerApplication\
ren prokan-pf.new prokan-pf

timeout /t 10

echo "Angular program Running"

@REM Creating Virtual Environment for dependencies
echo "Creating virtual environment"
cd C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\env\Scripts

echo "activating virtual environment"
call activate
echo "Python virtual environment Running"

echo "Creating log folder"
cd C:\Users\Administrator\Desktop\prokan-pf-bat-file\project\prokanpf\ServerApplication\prokanpf_server 
echo "Run server.py file"
start python server.py

cd C:\Program Files\nginx-1.20.1
start nginx -s reload